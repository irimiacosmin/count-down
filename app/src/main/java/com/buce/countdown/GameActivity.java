package com.buce.countdown;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.Random;

enum State
{
    START, INGAME, FINISHED;
}

public class GameActivity extends AppCompatActivity {

    String START_NEW_GAME = "Start new game";
    int WIN_COLOR = Color.parseColor("#00FF00");
    int LOSE_COLOR = Color.parseColor("#FF0000");
    int WOW_COLOR = Color.parseColor("#0000FF");
    int ELSE_COLOR = Color.parseColor("#FFFFFF");

    Button button;
    State state;
    long number;
    Integer counter;
    Thread mainThread;
    Integer highestScore = -1;

    private String yourScore(Integer score){
        if(score > highestScore){
            highestScore = score;
            button.setBackgroundColor(WOW_COLOR);
            return "Congats! New High Score " + score;
        }
        button.setBackgroundColor(WIN_COLOR);
        return "Your score is " + score;
    }

    private String getHighScore(){
        if(highestScore > 0){
            return "High score is " + highestScore;
        }
        return "Please win the game at least once.";
    }

    private View.OnClickListener buttonClickListener (){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (state){
                    case START:{
                        state = State.INGAME;
                        startGame();
                        break;
                    }
                    case INGAME:{
                        state = State.FINISHED;
                        endGame();
                        break;
                    }
                    case FINISHED:{
                        break;
                    }
                }
            }
        };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        button = findViewById(R.id.button);
        button.setOnClickListener(buttonClickListener());
        state = State.START;

        mainThread = getThread();
    }

    private void newGame(){
        mainThread = getThread();
        button.setBackgroundColor(ELSE_COLOR);
        setText(START_NEW_GAME);
        state = State.START;
    }
    private void setText(final String value){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                button.setText(value);
            }
        });
    }

    private Thread getThread(){
        return new Thread() {
            public void run() {
                try {
                    Random generator = new Random();
                    counter = 5;
                    setText(Integer.toString(counter));
                    number = (long)(generator.nextDouble() * 1000 + 500);
                    while(counter >= 0){
                        Thread.sleep(number);
                        if(counter > 1)
                            setText(Integer.toString(counter));
                        else
                            setText("??");
                        counter--;
                    }
                    setText("YOU LOST");
                    state = com.buce.countdown.State.FINISHED;
                    button.setBackgroundColor(LOSE_COLOR);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private void startGame(){
        mainThread.start();
    }

    private void endGame(){
        mainThread.interrupt();
        if(counter > 1){
            setText("You get a big 0!");
        }else{
            setText(yourScore(1500 - (int) number));
        }
    }

    private void highScore(){
        setText(getHighScore());
    }

    private void share(){
        Intent myIntent = new Intent(Intent.ACTION_SEND);
        myIntent.setType("text/plain");
        String shareBody = "I just passed " + highestScore + " on CountDown Game";
        String shareSub = "Wow!";
        myIntent.putExtra(Intent.EXTRA_SUBJECT, shareBody);
        myIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(myIntent, "Share using"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.newGame: {
                newGame();
                return true;
            }case R.id.highScore: {
                highScore();
                return true;
            }case R.id.share: {
                share();
                return true;
            }default:
                return super.onOptionsItemSelected(item);
        }
    }

}
